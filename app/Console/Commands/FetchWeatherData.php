<?php

namespace App\Console\Commands;

use App\Jobs\WeatherDataFetchJob;
use App\Models\City;
use Illuminate\Console\Command;


class FetchWeatherData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fetch:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will fetch data from https://openweathermap.org using free membership';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $cities = City::all();

        foreach ($cities as $city)
        {
            dispatch(new WeatherDataFetchJob($city));
        }
    }
}
