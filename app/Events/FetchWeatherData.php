<?php

namespace App\Events;

use App\Models\City;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class FetchWeatherData
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $city_id;

    public $city;

    public $country;

    public $from;

    public $to;

    /**
     * @param City $city
     * @param $from
     * @param $to
     */
    public function __construct(City $city, $from, $to)
    {
        $this->city_id = $city->id;
        $this->city    = $city->name;
        $this->country = $city->country;
        $this->from    = $from;
        $this->to      = $to;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
