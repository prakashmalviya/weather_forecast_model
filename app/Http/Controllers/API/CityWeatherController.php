<?php

namespace App\Http\Controllers\API;

use App\Events\FetchWeatherData;
use App\Models\City;
use App\Models\CityWeather;
use App\Http\Requests\UpdateCityWeatherRequest;
use App\Http\Resources\CityWeatherResource;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Symfony\Component\HttpFoundation\Response;
use Validator;


class CityWeatherController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $validator = Validator::make(request()->all(), [
            'from' => 'required|date',
            'to'   => 'required|date'
        ]);

        if ($validator->fails())
        {
            return response()->json(['error' => $validator->errors()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $start    = strtotime(request('from') . ' 00:00:00');
        $end      = strtotime(request('to') . ' 23:59:00');
        $cities   = City::all();
        $city_ids = [];

        try
        {
            //Let fine city wise data first
            foreach ($cities as $city)
            {
                $weather_data = CityWeather::whereCityId($city->id)->where('dt', '>=', $start)->where('dt', '<=', $end)->get();
                if ($weather_data->count() == 0)
                {
                    // If data not found for particular date range then lets fire event to fetch data for particular date range
                    FetchWeatherData::dispatch($city, $start, $end);
                }
                $city_ids[] = $city->id;
            }
            // Lets fetch from db for particular date range.
            $weather_data = CityWeather::whereIn('city_id', $city_ids)->where('dt', '>=', $start)->where('dt', '<=', $end)->get();

            if ($weather_data->count() > 0)
            {
                return new CityWeatherResource($weather_data);
            }
            else
            {
                return response()->json(['error' => trans('city_weather.no-record-found')], Response::HTTP_BAD_REQUEST);
            }
        }
        catch (\Exception $e)
        {
            return response()->json(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CityWeather $cityWeather
     * @return \Illuminate\Http\Response
     */
    public function show(CityWeather $cityWeather)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CityWeather $cityWeather
     * @return \Illuminate\Http\Response
     */
    public function edit(CityWeather $cityWeather)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateCityWeatherRequest $request
     * @param  \App\Models\CityWeather $cityWeather
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCityWeatherRequest $request, CityWeather $cityWeather)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CityWeather $cityWeather
     * @return \Illuminate\Http\Response
     */
    public function destroy(CityWeather $cityWeather)
    {
        //
    }
}
