<?php

namespace App\Listeners;

use App\Models\CityWeather;
use App\Services\OpenWeatherMap\Client;
use App\Events\FetchWeatherData;
use Carbon\Carbon;

class FetchWeatherDataFromAPI
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\FetchWeatherData $event
     * @return void
     */
    public function handle(FetchWeatherData $event)
    {
        $client = new Client();

        if (Carbon::parse($event->from)->format('Y-m-d') == Carbon::parse($event->to)->format('Y-m-d') && Carbon::now()->format('Y-m-d') == Carbon::parse($event->to)->format('Y-m-d'))
        {
            $weather_data = $client->getWeatherData($event->city, $event->country);

            if ($weather_data !== false)
            {
                CityWeather::create([
                    'city_id'     => $event->city_id,
                    'main'        => $weather_data->weather[0]->main,
                    'description' => $weather_data->weather[0]->description,
                    'base'        => $weather_data->base,
                    'temp'        => $weather_data->main->temp,
                    'feel_like'   => $weather_data->main->feels_like,
                    'temp_min'    => $weather_data->main->temp_min,
                    'temp_max'    => $weather_data->main->temp_max,
                    'pressure'    => $weather_data->main->pressure,
                    'humidity'    => $weather_data->main->humidity,
                    'visibility'  => $weather_data->visibility,
                    'wind_speed'  => $weather_data->wind->speed,
                    'wind_deg'    => $weather_data->wind->deg,
                    'dt'          => $weather_data->dt,
                ]);

            }
        }
        else
        {
            $weather_data = $client->getWeatherDataOnRange($event->city, $event->country, $event->from, $event->to);

            if ($weather_data !== false)
            {
                foreach ($weather_data as $data)
                {
                    CityWeather::create([
                        'city_id'     => $event->city_id,
                        'main'        => $data->weather[0]->main,
                        'description' => $data->weather[0]->description,
                        'base'        => $data->base,
                        'temp'        => $data->main->temp,
                        'feel_like'   => $data->main->feels_like,
                        'temp_min'    => $data->main->temp_min,
                        'temp_max'    => $data->main->temp_max,
                        'pressure'    => $data->main->pressure,
                        'humidity'    => $data->main->humidity,
                        'visibility'  => $data->visibility,
                        'wind_speed'  => $data->wind->speed,
                        'wind_deg'    => $data->wind->deg,
                        'dt'          => $data->dt,
                    ]);
                }
            }
        }
    }
}
