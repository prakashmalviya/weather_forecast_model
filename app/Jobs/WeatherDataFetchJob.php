<?php

namespace App\Jobs;

use App\Models\City;
use App\Models\CityWeather;
use App\Services\OpenWeatherMap\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class WeatherDataFetchJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $city;

    private $country;

    private $city_id;

    /**
     * @param City $city
     */
    public function __construct(City $city)
    {
        $this->city    = $city->name;
        $this->country = $city->country;
        $this->city_id = $city->id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $client = new Client();

        $weather_data = $client->getWeatherData($this->city, $this->country);

        if ($weather_data != false)
        {
            CityWeather::create([
                'city_id'     => $this->city_id,
                'main'        => $weather_data->weather[0]->main,
                'description' => $weather_data->weather[0]->description,
                'base'        => $weather_data->base,
                'temp'        => $weather_data->main->temp,
                'feel_like'   => $weather_data->main->feels_like,
                'temp_min'    => $weather_data->main->temp_min,
                'temp_max'    => $weather_data->main->temp_max,
                'pressure'    => $weather_data->main->pressure,
                'humidity'    => $weather_data->main->humidity,
                'visibility'  => $weather_data->visibility,
                'wind_speed'  => $weather_data->wind->speed,
                'wind_deg'    => $weather_data->wind->deg,
                'dt'          => $weather_data->dt,
            ]);
        }
    }
}
