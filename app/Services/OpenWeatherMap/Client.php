<?php

namespace App\Services\OpenWeatherMap;

use Psr\Log\LoggerInterface;
use GuzzleHttp;

class Client implements ClientInterface
{

    protected $logger;

    protected $httpClient;

    protected $apiId;

    protected $url;

    protected $historyDataUrl;

    public function __construct()
    {
        $this->apiId          = env('OPEN_WEATHER_APP_ID');
        $this->url            = env('OPEN_WEATHER_ENDPOINT');
        $this->historyDataUrl = env('OPEN_WEATHER_HISTORY_DATA_ENDPOINT');
    }

    /**
     * @param LoggerInterface $logger
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }


    /**
     * @return GuzzleHttp\Client
     */
    protected function getApiClient()
    {
        $stack = GuzzleHttp\HandlerStack::create();
        if ($this->logger instanceof LoggerInterface)
        {
            $stack->push(GuzzleHttp\Middleware::log(
                $this->logger,
                new GuzzleHttp\MessageFormatter(GuzzleHttp\MessageFormatter::DEBUG)
            ));
        }
        $this->httpClient = new GuzzleHttp\Client([
            'handler' => $stack,
        ]);

        return $this->httpClient;
    }

    /**
     * @param $city
     * @param $country
     * @return array|bool|float|int|mixed|null|object|string
     */
    public function getWeatherData($city, $country)
    {
        $apiClient = $this->getApiClient();
        try
        {
            $url          = $this->url . '?appid=' . $this->apiId . '&q=' . $city . ',' . $country;
            $apiResponse  = $apiClient->get($url, ['verify' => false]);
            $responseData = json_decode($apiResponse->getBody());
            if (isset($responseData))
            {
                return $responseData;
            }

            return false;
        }
        catch (\Exception $e)
        {
            return false;
        }
    }

    /**
     * @param $city
     * @param $country
     * @param $from
     * @param $to
     * @return array|bool|float|int|mixed|null|object|string
     */
    public function getWeatherDataOnRange($city, $country, $from, $to)
    {
        $apiClient = $this->getApiClient();
        try
        {
            $url = $this->historyDataUrl . '?appid=' . $this->apiId . '&q=' . $city . ',' . $country . '&start=' . $from . '&to=' . $to;
            $apiResponse  = $apiClient->get($url, ['verify' => false]);
            $responseData = json_decode($apiResponse->getBody());
            if (isset($responseData))
            {
                return $responseData;
            }

            return false;
        }
        catch (\Exception $e)
        {
            return false;
        }
    }


}
