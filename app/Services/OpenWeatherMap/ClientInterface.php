<?php

namespace App\Services\OpenWeatherMap;

use Psr\Log\LoggerAwareInterface;

/**
 * This class describes interface required from each trade platform for affiliate's integration.
 *
 * Interface ClientInterface
 * @package bof\Platforms\Trading
 */
interface ClientInterface extends LoggerAwareInterface
{
    /**
     * @param $city
     * @param $country
     * @return mixed
     */
    public function getWeatherData($city, $country);
}
