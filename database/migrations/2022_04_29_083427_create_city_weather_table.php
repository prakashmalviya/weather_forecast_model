<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCityWeatherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('city_weather', function (Blueprint $table)
        {
            $table->id();
            $table->bigInteger('city_id')->unsigned();
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');
            $table->string('main')->nullable();
            $table->string('description')->nullable();
            $table->string('base')->nullable();
            $table->string('temp')->nullable();
            $table->string('feel_like')->nullable();
            $table->string('temp_min')->nullable();
            $table->string('temp_max')->nullable();
            $table->string('pressure')->nullable();
            $table->string('humidity')->nullable();
            $table->string('visibility')->nullable();
            $table->string('wind_speed')->nullable();
            $table->string('wind_deg')->nullable();
            $table->string('dt')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('city_weather');
    }
}
