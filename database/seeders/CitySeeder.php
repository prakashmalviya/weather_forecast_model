<?php

namespace Database\Seeders;

use App\Models\City;
use Illuminate\Database\Seeder;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cities = ['United_State_of_America' => 'New York', 'United Kingdom' => 'London', 'France' => 'Paris', 'Germany' => 'Berlin', 'Japan' => 'Tokyo'];

        foreach ($cities as $country => $city)
        {
            $city_detail = City::whereName($city)->first();
            if (!$city_detail)
            {
                City::create([
                    'name'    => $city,
                    'country' => str_replace('_', ' ', $country)
                ]);
            }
        }

    }
}
