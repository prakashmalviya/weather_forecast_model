# Weather Forecasting Model

Laravel based load application with RestfulAPI

## Dependency

1. PHP 7.4+
2. Laravel 8
3. MySql 6+

## Installation

1. Checkout this repo on your location machine and run below commands.
2. Create your MySQL Database
3. Inside your .env file set following variables
```bash
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=

OPEN_WEATHER_APP_ID=
OPEN_WEATHER_ENDPOINT=https://api.openweathermap.org/data/2.5/weather
OPEN_WEATHER_HISTORY_DATA_ENDPOINT=http://history.openweathermap.org/data/2.5/history/city

```
After that run below commands

```bash
composer install
php artisan migrate
php artisan db:seed
php artisan fetch:data
php artisan serve
```

After Serve command you will see the built in host URL which is you have to set as your postman environment variable host. Mostly like
http://127.0.0.1:8000/

## Running Unit Tests

To run tests, run the following command

```bash
  php artisan test
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
