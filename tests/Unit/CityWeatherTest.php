<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Carbon;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;
use Tests\TestCase;

class CityWeatherTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_fetch_today_data()
    {
        $this->json('get', route('fetch-data', ['from' => Carbon::now()->format('Y-m-d'), 'to' => Carbon::now()->format('Y-m-d')]))
             ->assertStatus(ResponseAlias::HTTP_OK)
             ->assertJsonStructure(
                 [
                     'data' => [
                         '*' => [
                             'id',
                             'city_id',
                             'main',
                             'description',
                             'base',
                             'temp',
                             'feel_like',
                             'temp_min',
                             'temp_max',
                             'pressure',
                             'humidity',
                             'visibility',
                             'wind_speed',
                             'wind_deg',
                             'dt',
                             'created_at',
                             'updated_at',
                         ]
                     ]
                 ]
             )->json();

    }

    public function test_fetch_historical_data()
    {
        $this->json('get', route('fetch-data', ['from' => Carbon::now()->subDay(8)->format('Y-m-d'), 'to' => Carbon::now()->subDay(1)->format('Y-m-d')]))
             ->assertStatus(ResponseAlias::HTTP_OK)
             ->assertJsonStructure(
                 [
                     'data' => [
                         '*' => [
                             'id',
                             'city_id',
                             'main',
                             'description',
                             'base',
                             'temp',
                             'feel_like',
                             'temp_min',
                             'temp_max',
                             'pressure',
                             'humidity',
                             'visibility',
                             'wind_speed',
                             'wind_deg',
                             'dt',
                             'created_at',
                             'updated_at',
                         ]
                     ]
                 ]
             )->json();

    }
}
